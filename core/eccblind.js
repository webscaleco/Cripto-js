// eVotUM - Electronic Voting System
//
// eccblind.js
//
// Cripto-7.0.0 - Blind signatures
//
// Copyright (c) 2016 Universidade do Minho
// Developed by André Baptista - Devise Futures, Lda. (andre.baptista@devisefutures.com)
// Reviewed by Ricardo Barroso - Devise Futures, Lda. (ricardo.barroso@devisefutures.com)
//
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//

var curveC256 = sjcl.ecc.curves.c256;
var CURVE_N = curveC256.r;
var CURVE_G = curveC256.G;

//math functions
evotum.eccblind.getRandomInRange = function(minInclusive, maxInclusive) {
  if (maxInclusive.greaterEquals(minInclusive) === 0) {
    return null;
  }
  return sjcl.bn.random(maxInclusive).add(minInclusive);
}

evotum.eccblind.getCurveOrderRandom = function() {
  return evotum.eccblind.getRandomInRange(1, CURVE_N.sub(1));
}

evotum.eccblind.init = function() {
  var k, pRDash, rDash;
  while (true) {
    k = evotum.eccblind.getCurveOrderRandom();
    pRDash = CURVE_G.mult(k);
    rDash = pRDash.x.mod(CURVE_N);
    if (rDash !== 0) {
      return {"pRDash": pRDash, "rDash": rDash, "k": k};
    }
  }
}

//not required but still implemented
evotum.eccblind.blind = function(pRDash, h) {
  var rDash = pRDash.x.mod(CURVE_N);

  var v = evotum.eccblind.getCurveOrderRandom();
  var pR = pRDash.mult(v.inverseMod(CURVE_N));
  var r = pR.x.mod(CURVE_N);

  var blindM = (h.mul(r.inverseMod(CURVE_N)).mul(rDash).mul(v)).mod(CURVE_N);
  return {"blindM": blindM, "v": v, "pR": pR, "r": r};
}

evotum.eccblind.sign = function(d, blindM, rDash, k) {
  return (d.mul(blindM)).add((k.mul(rDash)).mod(CURVE_N));
}

evotum.eccblind.unblind = function(s, v, rDash, r) {
  return (s.mul(v.inverseMod(CURVE_N)).mul(rDash.inverseMod(CURVE_N).mul(r))).mod(CURVE_N);
}

evotum.eccblind.verify = function(sDash, pointQ, h, pR, r) {
  var pointA = pointQ.mult(h).toJac().add(pR.mult(r)).toAffine();
  var pointB = CURVE_G.mult(sDash);
  return (pointA.x.equals(pointB.x) && pointA.y.equals(pointB.y))
}

//main functions
//Cripto 7.0.2
evotum.eccblind.initSigner = function() {
//  Initialization of the blind signature scheme, according to section IV.A of the
//  paper followed in this implementation.
//
//  Args:
//      none
//  Returns:
//      {initComponents (str), pRDashComponents (str)}: initComponents = "r' (hex) . k (hex)" and
//          pRDashComponents = "R' x coordinate (hex) . R' y coordinate (hex)"
  var result = evotum.eccblind.init();
  var pRDash = result["pRDash"];
  var rDash = result["rDash"];
  var k = result["k"];

  return {"initComponents": evotum.eccblind.pack([rDash, k]), "pRDashComponents": evotum.eccblind.pack([pRDash.x, pRDash.y])};
}

//Cripto 7.1.2
evotum.eccblind.blindData = function(pRDashComponents, data) {
//  Blinds data, according to section IV.B of the paper followed in this implementation.
//
//  Args:
//      pRDashComponents (str): pRDashComponents returned by evotum.eccblind.initSigner()
//      data (str): data to be blinded
//  Returns:
//      {errorCode (int/null), blindComponents (str/null), pRComponents (str/null), blindM (hex/null)}: if errorCode
//          is null, the other components have the following values: blindComponents = "v (hex) . r (hex)",
//          pRComponents = "R x coordinate (hex) . R y coordinate (hex)" and BlindM = m' (hex). If
//          errorCode is not null, the other comonents are null.
//          The errorCode has the following meaning:
//              1 - invalid format of pRDashComponents
  var pRDash = evotum.eccblind.unpackEccPoint(pRDashComponents);
  if (pRDash === null) {
    return {"errorCode": 1, "blindComponents": null, "pRComponents": null, "blindM": null};
  }

  var h = new sjcl.bn(evotum.hashfunctions.generateSHA256Hash(data));

  var result = evotum.eccblind.blind(pRDash, h);
  var blindM = result["blindM"];
  var v = result["v"];
  var pR = result["pR"];
  var r = result["r"];

  return {"errorCode": null, "blindComponents": evotum.eccblind.pack([v, r]), "pRComponents": evotum.eccblind.pack([pR.x, pR.y]), "blindM": evotum.eccblind.bnToHex(blindM)};
}

//Cripto 7.3.2
evotum.eccblind.unblindSignature = function(blindSignature, pRDashComponents, blindComponents) {
//  Unblinds blindSignature, according to section IV.D of the paper followed in this implementation,
//  meaning that this function returns the signature of data (see evotum.eccblind.blindData()) by pemKey (see generateBlindSignature())
//
//  Args:
//      blindSignature (hex): blind signature that will be unblinded, returned by generateBlindSignature()
//      pRDashComponents (str): pRDashComponents returned by evotum.eccblind.initSigner()
//      blindComponents (str): blindComponents returned by evotum.eccblind.blindData()
//  Returns:
//      {errorCode (int/null), signature (hex/Nonullne)}: tuple with errorCode and
//      unblinded blindSignature, meaning the signature of data (see evotum.eccblind.blindData()) by pemKey (see generateBlindSignature()).
//          The errorCode has the following meaning:
//              1 - invalid format of pRDashComponents
//              2 - invalid format of blindComponents
//              3 - invalid format of blindSignature
//          If the errorCode is not null, signature will be null
  var pRDash = evotum.eccblind.unpackEccPoint(pRDashComponents);
  if (pRDash === null) {
    return {"errorCode": 1, "signature": null};
  }

  var result = evotum.eccblind.unpack(blindComponents);
  if (result === null || result.length != 2) {
    return {"errorCode": 2, "signature": null};
  }

  var v = result[0];
  var r = result[1];

  var rDash = pRDash.x.mod(CURVE_N);

  if (!evotum.eccblind.isHexString(blindSignature)) {
    return {"errorCode": 3, "signature": null};
  }

  return {"errorCode": null, "signature": evotum.eccblind.bnToHex(evotum.eccblind.unblind(new sjcl.bn(blindSignature), v, rDash, r))};
}

//Cripto 7.4.2
evotum.eccblind.verifySignature = function(pemPublicKey, signature, blindComponents, pRComponents, data) {
//  Verify signature of data, according to section IV.E of the paper followed in this implementation,
//  meaning that this function verifies if signature is the signature of data
//  Note that to get the public key from a certificate, you should use evotum.eccblind.getEccPublicKeyFromCertificate(pemCertificate)
//
//  Args:
//      pemPublicKey (PEM): signer's public key in PEM format
//      signature (hex): signature to verify, returned by evotum.eccblind.unblindSignature()
//      blindComponents (str): blindComponents returned by evotum.eccblind.blindData()
//      pRComponents (str): pRDashComponents returned by evotum.eccblind.blindData()
//      data (str): data blinded in evotum.eccblind.blindData()
//  Returns:
//      {errorCode (int/null), result (bool/null)}: tuple with errorCode and result (True if signature is
//      the signature of data; False otherwise).
//          The errorCode has the following meaning:
//              1 - invalid format of pRComponents
//              2 - invalid format of blindComponents
//              3 - invalid format of signature
//              4 - invalid format of pemPublicKey
//          If the errorCode is not null, result will be null
  var publicKey = evotum.eccblind.importPemEccPublicKey(pemPublicKey);

  if (publicKey["errorCode"] === 1) {
    return {"errorCode": 4,  "result": null};
  }

  var pR = evotum.eccblind.unpackEccPoint(pRComponents);
  if (pR === null) {
    return {"errorCode": 1,  "result": null};
  }

  var h = new sjcl.bn(evotum.hashfunctions.generateSHA256Hash(data));

  var result = evotum.eccblind.unpack(blindComponents);
  if (result === null || result.length != 2) {
    return {"errorCode": 2,  "result": null};
  }

  var v = result[0];
  var r = result[1];

  if (!evotum.eccblind.isHexString(signature)) {
    return {"errorCode": 3,  "result": null};
  }

  return {"errorCode": null, "result": evotum.eccblind.verify(new sjcl.bn(signature), publicKey.pointQ, h, pR, r)};
}

//utils
evotum.eccblind.importPemEccPublicKey = function(pemEccPublicKey) {
  try {
    var decodedKey = forge.pem.decode(pemEccPublicKey);
    var body = decodedKey[0].body;
    var bitArray = sjcl.codec.base64.toBits(forge.util.encode64(body));

    /*Since the available JS libraries still don't implement EC keys parsing,
    the key is parsed manually. These are the correct offsets for this curve.*/
    var xParam = curveC256.field.fromBits(sjcl.bitArray.bitSlice(bitArray, 27*8, 59*8));
    var yParam = curveC256.field.fromBits(sjcl.bitArray.bitSlice(bitArray, 59*8, 91*8));

    if (xParam.equals(0) || yParam.equals(0)) {
      return {"errorCode": 1};
    }
    return {"errorCode": null, "curve": "P-256", "pointQ": new sjcl.ecc.point(curveC256, xParam, yParam)};
  }
  catch (error) {
    return {"errorCode": 1};
  }
}

evotum.eccblind.getEccPublicKeyFromCertificate = function(pemCertificate) {
  try {
    var decodedCert = forge.pem.decode(pemCertificate);
    var body = decodedCert[0].body;
    var object = forge.asn1.fromDer(body);

    var sequenceLength = object.value[0].value.length;

    var header, headerA, headerB, rawKey;
    for (var i=0;i<sequenceLength;i++) {
      try {
        header = object.value[0].value[i].value[0];
        headerA = header.value[0].value;
        headerB = header.value[1].value;
        rawKey = object.value[0].value[i].value[1].value;

        //>= 520 bits
        if (rawKey.length >= 65) {
          break;
        }
      }
      catch (error) { }
    }

    if (rawKey === null) {
      return {"errorCode": 1, "publicKey": null};
    }

    /*Since the available JS libraries still don't support EC key extraction from
    certificates, the key is extracted manually. These are the correct values
    for this curve.*/
    var finalDERSequence = "\x30\x59\x30\x13\x06\x07" + headerA + "\x06\x08" + headerB + "\x03\x42" + rawKey;
    var pemEccPublicKey = "-----BEGIN PUBLIC KEY-----\n" + forge.util.encode64(finalDERSequence) + "\n-----END PUBLIC KEY-----";

    return {"errorCode": null, "publicKey": pemEccPublicKey};
  }
  catch (error) {
    return {"errorCode": 1, "publicKey": null};
  }
}

evotum.eccblind.pack = function(arr) {
  var hexArray = [];
  for (var i=0;i<arr.length;i++) {
    hexArray[i] = evotum.eccblind.bnToHex(arr[i]);
  }
  return hexArray.join(".");
}

evotum.eccblind.unpack = function(s) {
  arr = s.split(".");
  var l = [];
  for (var i=0;i<arr.length;i++) {
    try {
      if (!evotum.eccblind.isHexString(arr[i])) {
        return null;
      }
      l[i] = new sjcl.bn(arr[i]);
    }
    catch (error) {
      return null;
    }
  }
  return l;
}

evotum.eccblind.unpackEccPoint = function(s) {
  l = evotum.eccblind.unpack(s);

  if (l === null || l.length != 2) {
    return null;
  }

  return new sjcl.ecc.point(curveC256, l[0], l[1]);
}

evotum.eccblind.bnToHex = function(bn) {
  return bn.toString().substr(2);
}

evotum.eccblind.isHexString = function(s) {
  var re = /^[0-9A-Fa-f]+$/g;
  return re.test(s);
}
